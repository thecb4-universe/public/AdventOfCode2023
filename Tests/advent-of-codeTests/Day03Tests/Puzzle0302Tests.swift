import XCTest

@testable import advent_of_code

final class Puzzle0302Tests: XCTestCase {

  func testSampleSchematic() async throws {
    let data = Self.Fixtures.sampleSchematicData
    var schematic = Schematic(data: data)
    let expected = 467835

    let gears = schematic.adjacentNumberMap.filter { $0.isGear }

    let powers = gears.map { $0.power }

    let power = powers.reduce(0, +)

    let actual = power

    print(gears.map { $0.description }.joined(separator: "\n"))
    print(powers)
    print(power)

    XCTAssertEqual(expected, actual)

  }

  func testPuzzleData() async throws {
    // given
    guard let data = Self.Fixtures.day03Data else {
      XCTFail("Could Not Get Puzzle Data")
      return
    }

    var schematic = Schematic(data: data)
    let expected = 69_527_306  // correect answer

    let gears = schematic.adjacentNumberMap.filter { $0.isGear }

    let powers = gears.map { $0.power }

    let power = powers.reduce(0, +)

    let actual = power

    print(gears.map { $0.description }.joined(separator: "\n"))
    print("powers are: \(powers)")
    print("sum of powers: \(power)")

    XCTAssertEqual(expected, actual)

  }

}
