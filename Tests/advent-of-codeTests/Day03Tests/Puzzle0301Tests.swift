import XCTest

@testable import advent_of_code

final class Puzzle0301Tests: XCTestCase {

  func testSampleSchematic() async throws {
    let data = Self.Fixtures.sampleSchematicData
    var schematic = Schematic(data: data)
    let expected = 4361

    let symbolMap = schematic.symbolMap

    let numberMap = schematic.numberMap

    let adjacencyMap: SchematicAdjacencies = schematic.adjacentNumberMap

    let parts = schematic.partNumbers

    let sum = schematic.partNumbersSum

    let actual = sum

    // print("--------- Summary (Sample Schematic) ---------")
    // print("width: \(schematic.width)")
    // print("height: \(schematic.width)")
    // print()
    // print("symbols: \n\(symbolMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("numbers: \n\(numberMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("adjacencies: \n\(adjacencyMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("parts found: \n\(parts.map { $0.description }.joined(separator: "\n"))")
    // print("sum: \(sum)")
    // print("----------------------------------------------")

    XCTAssertEqual(actual, expected)

  }

  func testFirst05LinesOfSchematicData() async {
    let data = Self.Fixtures.first05LinesOfSchematicData
    var schematic = Schematic(data: data)
    let expected = 19078

    let symbolMap = schematic.symbolMap

    let numberMap = schematic.numberMap

    let adjacencyMap: SchematicAdjacencies = schematic.adjacentNumberMap

    let parts = schematic.partNumbers

    let sum = schematic.partNumbersSum

    let actual = sum

    // print("--------- Summary (First 5 lines of Real Schematic) ---------")
    // print("width: \(schematic.width)")
    // print("height: \(schematic.width)")
    // print()
    // print("symbols: \n\(symbolMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("numbers: \n\(numberMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("adjacencies: \n\(adjacencyMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("parts found: \n\(parts.map { $0.description }.joined(separator: "\n"))")
    // print("sum: \(sum)")
    // print("------------------------------------------------------------")

    XCTAssertEqual(actual, expected)
  }

  func testSlurpNumbers() async {
    let data = Self.Fixtures.sampleSchematicData
    var schematic = Schematic(data: data)

    var priorValueDigit = false

    var count = 0

    var hash: [Int: [[String: Int]]] = [:]

    var set: [[String: Int]] = []

    for (index, detail) in schematic.details.enumerated() {

      if detail.isNumber && !priorValueDigit {
        set.append([detail: index])
      } else {
        priorValueDigit = false
        hash[count] = set
        set = []
        count += 1
      }

      if index == schematic.details.count - 1 && !set.isEmpty {
        hash[count] = set
      }

    }

    let numbers =
      hash
      .filter { !$0.value.isEmpty }
      .sorted(by: { $0.0 < $1.0 })
      .map { $0.value }.map { $0.flatMap { $0.map { $0.key } }.joined() }

    // print(numbers)

    let ranges =
      hash
      .filter { !$0.value.isEmpty }
      .sorted(by: { $0.0 < $1.0 })
      .map { $0.value }.map { $0.flatMap { $0.map { $0.value } } }.map {
        $0.first!..<($0.last! + 1)
      }

    // print(ranges)

    guard let range = ranges.first else {
      XCTFail()
      return
    }

    let numberMap = zip(ranges, numbers).map { range, number in
      SchematicNumber(value: number, range: range)
    }

    let adjacencies = schematic.searchMap.map { search in
      let found = search.space.map { point in
        numberMap.filter { $0.range ~= point }
      }.filter { $0.isEmpty == false }.flatMap { $0 }

      return (search.symbol, Array(Set(found)))
    }.map { (symbol, numbers) in
      SchematicAdjacency(symbol: symbol, numbers: numbers)
    }

  }

  func testPuzzleData() async throws {
    // given
    guard let data = Self.Fixtures.day03Data else {
      XCTFail("Could Not Get Puzzle Data")
      return
    }

    let expected = 521515  // correct answer
    // wrong answers
    // 1634187

    // when
    var schematic = Schematic(data: data)

    guard schematic.width > 0 else {
      XCTFail("Failed to get schematic width")
      return
    }

    guard schematic.height > 0 else {
      XCTFail("Failed to get schematic height")
      return
    }

    XCTAssertEqual(schematic.width, 140)
    XCTAssertEqual(schematic.height, 140)

    let symbolMap = schematic.symbolMap

    let numberMap = schematic.numberMap

    let adjacencyMap: SchematicAdjacencies = schematic.adjacentNumberMap

    let parts = schematic.partNumbers

    let sum = schematic.partNumbersSum

    let actual = sum

    // then
    XCTAssertEqual(actual, expected)

    // print("--------- Summary (Real Schematic) ---------")
    // print("width: \(schematic.width)")
    // print("height: \(schematic.width)")
    // print()
    // print("symbols: \n\(symbolMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("numbers: \n\(numberMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("adjacencies: \n\(adjacencyMap.map { $0.description }.joined(separator: "\n"))")
    // print()
    // print("parts found: \n\(parts.map { $0.description }.joined(separator: "\n"))")
    // print("sum: \(sum)")
    // print("--------------------------------------------")

    XCTAssertEqual(actual, expected)

  }
}
