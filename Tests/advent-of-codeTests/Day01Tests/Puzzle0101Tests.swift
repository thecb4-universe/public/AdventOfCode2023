import XCTest

@testable import advent_of_code

final class Puzzle0101Tests: XCTestCase {
  func testCapturingNumbersFromCalibrationLine() throws {
    // given some line of text
    let line = "1abc2"
    let expected = [1, 2]

    // when
    let parser = LineParser()
    let actual = parser.parse(line)

    // then
    XCTAssertEqual(actual, expected)

  }

  func testConvertArrayOfNumbersToCombinedValue() throws {
    // given
    let numbers = [1, 2]
    let expected = 12

    // when
    let transformer = LineTransformer()
    let actual = transformer.transform(numbers)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testParseParagraphToLines() throws {
    // given
    let paragraph =
      """
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      """
    let expected = [
      "1abc2",
      "pqr3stu8vwx",
      "a1b2c3d4e5f",
      "treb7uchet",
    ]

    // when
    let splitter = ParagraphParser()
    let actual = splitter.parse(paragraph)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testConvertParagraphToArrayOfNumbers() throws {
    // given
    let paragraph =
      """
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      """
    let expected = [
      12,
      38,
      15,
      77,
    ]

    // when
    let paragraphParser = ParagraphParser()
    let parsedParagraph = paragraphParser.parse(paragraph)

    let paragraphTransformer = ParagraphTransformer()
    let transformedParagraph = paragraphTransformer.transform(parsedParagraph)

    let actual = transformedParagraph

    // then
    XCTAssertEqual(actual, expected)
  }

  func testEncapsulationOfParsingAndTransforming() {
    // given
    let text =
      """
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      """
    let expected = [
      12,
      38,
      15,
      77,
    ]

    // when
    let document = CalibrationDocument(using: text)
    let actual = document.values

    // then
    XCTAssertEqual(actual, expected)

  }

  func testSumOfCalibrationValues() throws {
    // given
    let text =
      """
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      """
    let expected = 142

    // when
    let document = CalibrationDocument(using: text)
    let actual = document.values.reduce(0, +)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testPuzzleData() throws {

    // given
    guard let text = try? Self.Fixtures.get(file: "puzzle0101.txt") else {
      XCTFail("Could Not Get Puzzle Data")
      return
    }
    let expected = 55538  // correct answer with current puzzle0101 file

    // when
    let document = CalibrationDocument(using: text)

    let values = document.values

    let actual = values.reduce(0, +)

    XCTAssertEqual(actual, expected)
  }
}
