import XCTest

@testable import advent_of_code

final class Puzzle0102Tests: XCTestCase {
  func testFindSingleTextIndexInString() throws {
    // given
    let string = "sixdddkcqjdnzzrgfourxjtwosevenhg9"
    let six = "six"
    let four = "four"
    let two = "two"
    let seven = "seven"
    let number9 = "9"

    let expectedIndexOfSix = String.Index(utf16Offset: 0, in: string)
    let expectedIndexOfFour = String.Index(utf16Offset: 16, in: string)
    let expectedIndexOfTwo = String.Index(utf16Offset: 22, in: string)
    let expectedIndexOfSeven = String.Index(utf16Offset: 25, in: string)
    let expectedIndexOfNumber9 = String.Index(utf16Offset: 32, in: string)

    // when
    let actualIndexOfSix = string.index(of: six)
    let actualIndexOfFour = string.index(of: four)
    let actualIndexOfTwo = string.index(of: two)
    let actualIndexOfSeven = string.index(of: seven)
    let actualIndexOfNumberNine = string.index(of: number9)

    // then
    XCTAssertEqual(actualIndexOfSix, expectedIndexOfSix)
    XCTAssertEqual(actualIndexOfFour, expectedIndexOfFour)
    XCTAssertEqual(actualIndexOfTwo, expectedIndexOfTwo)
    XCTAssertEqual(actualIndexOfSeven, expectedIndexOfSeven)
    XCTAssertEqual(actualIndexOfNumberNine, expectedIndexOfNumber9)
  }

  func testCalibrationValueTokenValueEnum() throws {
    // given
    let string = "sixdddkcqjdnzzrgfourxjtwosevenhg9"
    let expectedIndexOfSix = String.Index(utf16Offset: 0, in: string)

    // when
    let actualIndexOfSix = string.index(of: CalibrationValueToken.Number.six.rawValue)

    // then
    XCTAssertEqual(actualIndexOfSix, expectedIndexOfSix)
  }

  func testCreateCalibrationValueToken() {
    // given
    let string = "sixdddkcqjdnzzrgfourxjtwosevenhg9"
    let six = CalibrationValueToken.Number.six
    let expectedIndexOfSix = String.Index(utf16Offset: 0, in: string)

    // when
    let actualIndexOfSix = string.index(of: six.string)
    let token = CalibrationValueToken(number: six, index: expectedIndexOfSix)

    // then
    XCTAssertEqual(token.number, six)
    XCTAssertEqual(token.index, actualIndexOfSix)
  }

  func testCapturingTokensFromCalibrationLine() throws {

    // given some line of text
    let line = "1abc2"
    let expected = [
      CalibrationValueToken(
        number: CalibrationValueToken.Number.n1,
        index: String.Index(utf16Offset: 0, in: line)
      ),
      CalibrationValueToken(
        number: CalibrationValueToken.Number.n2,
        index: String.Index(utf16Offset: 4, in: line)
      ),
    ]

    // when
    let parser = TokenLineParser()
    let actual = parser.parse(line)

    // then
    XCTAssertEqual(actual, expected)

  }

  func testTransformTokensFromCalibrationLine() throws {
    // given
    let line = "1abc2"
    let expected = 12

    // when
    let parser = TokenLineParser()
    let tokens = parser.parse(line)

    // then
    let transformer = TokenLineTransformer()
    let actual = transformer.transform(tokens)

    XCTAssertEqual(actual, expected)

  }

  func testConvertParagraphToArrayOfNumbers() throws {
    // given
    let paragraph =
      """
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      """
    let expected = [
      12,
      38,
      15,
      77,
    ]

    // when
    let paragraphParser = ParagraphParser()
    let parsedParagraph = paragraphParser.parse(paragraph)

    let paragraphTransformer = TokenParagraphTransformer()
    let transformedParagraph = paragraphTransformer.transform(parsedParagraph)

    let actual = transformedParagraph

    // then
    XCTAssertEqual(actual, expected)
  }

  func testEncapsulationOfParsingAndTransforming() {
    // given
    let text =
      """
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
      """
    let expected = [
      12,
      38,
      15,
      77,
    ]

    // when
    let document = TokenCalibrationDocument(using: text)
    let actual = document.values

    // then
    XCTAssertEqual(actual, expected)

  }

  func testPuzzleData() throws {

    // given
    guard let text = try? Self.Fixtures.get(file: "puzzle0101.txt") else {
      XCTFail("Could Not Get Puzzle Data")
      return
    }
    let expected = 54875  // correct answer with current puzzle0102 file

    // when
    let document = TokenCalibrationDocument(using: text)

    let values = document.values

    let actual = values.reduce(0, +)

    XCTAssertEqual(actual, expected)
  }
}
