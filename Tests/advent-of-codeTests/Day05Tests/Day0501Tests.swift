import XCTest

@testable import advent_of_code

final class Puzzle0501Tests: XCTestCase {
  func testBreakingUpAlmanac() {
    let input = Self.Fixtures.Day05.sample

    // split based on space between maps (and seed line)
    var almanac = input.components(separatedBy: "\n\n")

    guard !almanac.isEmpty else {
      XCTFail()
      return
    }

    let seedLine = almanac.removeFirst()

    XCTAssertEqual(seedLine, Self.Fixtures.Day05.sampleSeedLine)

    // get just the seed number string
    guard let seedsInfo = seedLine.components(separatedBy: ":").dropFirst().last else {
      XCTFail()
      return
    }

    // parse seed string into integers
    let seeds = seedsInfo.components(separatedBy: " ").compactMap { Int($0) }

    XCTAssertEqual(seeds, [79, 14, 55, 13])

    let maps = almanac

    XCTAssertEqual(maps.count, 7)

    // lets work with the first map
    let map = maps[0]
    let mapLines = map.components(separatedBy: "\n")

    guard let headerLine = mapLines.first?.replacingOccurrences(of: ":", with: "") else {
      XCTFail()
      return
    }

    XCTAssertEqual(headerLine, Self.Fixtures.Day05.sampleMapHeader)

    let rangeLine1 = mapLines[1]

    let rangeInfo1 = rangeLine1.components(separatedBy: " ").compactMap { Int($0) }

    XCTAssertEqual(rangeInfo1, [50, 98, 2])

    let sourceRangeStart1 = rangeInfo1[1]
    let destinationRangeStart1 = rangeInfo1[0]
    let rangeLength1 = rangeInfo1[2]

    XCTAssertEqual(destinationRangeStart1, 50)
    XCTAssertEqual(sourceRangeStart1, 98)
    XCTAssertEqual(rangeLength1, 2)

    let sourceRangeEnd1 = sourceRangeStart1 + rangeLength1 - 1
    let destinationRangeEnd1 = destinationRangeStart1 + rangeLength1 - 1

    XCTAssertEqual(destinationRangeEnd1, 51)
    XCTAssertEqual(sourceRangeEnd1, 99)

    // -------

    let rangeLine2 = mapLines[2]

    let rangeInfo2 = rangeLine2.components(separatedBy: " ").compactMap { Int($0) }

    // [ destination, source, range]
    XCTAssertEqual(rangeInfo2, [52, 50, 48])

    let sourceRangeStart2 = rangeInfo2[1]
    let destinationRangeStart2 = rangeInfo2[0]
    let rangeLength2 = rangeInfo2[2]

    XCTAssertEqual(destinationRangeStart2, 52)
    XCTAssertEqual(sourceRangeStart2, 50)
    XCTAssertEqual(rangeLength2, 48)

    let sourceRangeEnd2 = sourceRangeStart2 + rangeLength2 - 1
    let destinationRangeEnd2 = destinationRangeStart2 + rangeLength2 - 1

    XCTAssertEqual(destinationRangeEnd2, 99)
    XCTAssertEqual(sourceRangeEnd2, 97)

  }

  func testParseSeeds() throws {
    // given
    let input = Self.Fixtures.Day05.sampleSeedLine
    let expected = [79, 14, 55, 13]

    // when
    let parser = AlmanacParser()
    let actual = try parser.parseSeeds(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testParseSourceRange() throws {

    // given
    let input = Self.Fixtures.Day05.sampleMapEntryLine
    let expected = 98...99

    // when
    let parser = AlmanacParser()
    let actual = try parser.parseSourceRange(input)

    // then
    XCTAssertEqual(actual, expected)

  }

  func testParseDestinationRange() throws {
    // given
    let input = Self.Fixtures.Day05.sampleMapEntryLine
    let expected = 50...51

    // when
    let parser = AlmanacParser()
    let actual = try parser.parseDestinationRange(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testParseAlmanacMapEntry() throws {
    // given
    let input = Self.Fixtures.Day05.sampleMapEntryLine
    let expected = AlmanacMapEntry(
      source: 98...99,
      destination: 50...51
    )

    // when
    let parser = AlmanacParser()
    let actual = try parser.parseMapEntry(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testHeaderEnum() throws {
    let input = Self.Fixtures.Day05.sampleMapHeader
    let expected = MapHeader.seed_soil

    // when
    let parser = AlmanacParser()
    let actual = try parser.parseHeader(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testParseAlamancMap() throws {

    let input = Self.Fixtures.Day05.sampleMap

    let expected = AlmanacMap(
      header: MapHeader.seed_soil,
      entries: [
        AlmanacMapEntry(
          source: 98...99,
          destination: 50...51
        ),
        AlmanacMapEntry(
          source: 50...97,
          destination: 52...99
        ),
      ]
    )

    // when
    let parser = AlmanacParser()
    let actual = try parser.parseMap(input)

    // then
    XCTAssertEqual(actual, expected)

  }

  func testMapFromToFound() throws {
    let input = Self.Fixtures.Day05.sampleMap
    let expected = 81
    let number = 79

    // when
    let parser = AlmanacParser()
    let map = try parser.parseMap(input)
    let actual = map.destination(from: number)

    // then
    XCTAssertEqual(actual, expected)

  }

  func testMapFromToNotFound() throws {
    let input = Self.Fixtures.Day05.sampleMap
    let expected = 10
    let number = 10

    // when
    let parser = AlmanacParser()
    let map = try parser.parseMap(input)
    let actual = map.destination(from: number)

    // then
    XCTAssertEqual(actual, expected)

  }

  func testParseAlmanac() throws {
    let input = Self.Fixtures.Day05.sample
    let seed = 79

    // when
    let parser = AlmanacParser()
    let almanac = try parser.parse(input)

    let soil = almanac.destination(from: seed, mapping: .seed_soil)

    let fertilizer = almanac.destination(from: soil, mapping: .soil_fertilizer)

    let water = almanac.destination(from: fertilizer, mapping: .fertilizer_water)

    let light = almanac.destination(from: water, mapping: .water_light)

    let temperature = almanac.destination(from: light, mapping: .light_temperature)

    let humidity = almanac.destination(from: temperature, mapping: .temperature_humidity)

    let location = almanac.destination(from: humidity, mapping: .humidity_location)

    // then
    XCTAssertEqual(soil, 81)
    XCTAssertEqual(fertilizer, 81)
    XCTAssertEqual(water, 81)
    XCTAssertEqual(light, 74)
    XCTAssertEqual(temperature, 78)
    XCTAssertEqual(humidity, 78)
    XCTAssertEqual(location, 82)

  }

  func testLocationForSeed() throws {
    // given
    let input = Self.Fixtures.Day05.sample
    let seed = 79
    let expected = 82

    // when
    let parser = AlmanacParser()
    let almanac = try parser.parse(input)
    let location = almanac.location(for: seed)
    let actual = location

    // then
    XCTAssertEqual(actual, expected)
  }

  func testMapSeedsToLocations() throws {
    // given
    let input = Self.Fixtures.Day05.sample

    let expected = [82, 43, 86, 35]

    // when
    let parser = AlmanacParser()
    let almanac = try parser.parse(input)
    let locations = almanac.seeds.map { almanac.location(for: $0) }
    let actual = locations

    // then
    XCTAssertEqual(actual, expected)
  }

  func testBestSeedGivenLocation() throws {
    // given
    let input = Self.Fixtures.Day05.sample
    let expected = 35

    // when
    let parser = AlmanacParser()
    let almanac = try parser.parse(input)
    let actual = almanac.bestSeed

    // then
    XCTAssertEqual(actual, expected)
  }

  func testPuzzleData() throws {

    guard let input = Self.Fixtures.Day05.puzzle else {
      XCTFail()
      return
    }

    let expected = 84470622 // correct answer

    // when
    let parser = AlmanacParser()
    let almanac = try parser.parse(input)
    let actual = almanac.bestSeed

    // then
    XCTAssertEqual(actual, expected)

  }
}
