import XCTest

@testable import advent_of_code

final class Puzzle0402Tests: XCTestCase {
  func testGetCopyIDs() {
    // given
    let winningNumbers = [
      41, 48, 83, 86, 17,
    ]

    let myNumbers = [
      83, 86, 6, 31, 17, 9, 48, 53,
    ]

    let card = ScratchCard(id: 1, winners: winningNumbers, game: myNumbers)

    let expected = [2, 3, 4, 5]

    // when
    let actual = card.winningCopies()

    // then
    XCTAssertEqual(actual, expected)
  }

  func testScratchCardCountWithCopiesForSampleData() throws {
    // given
    let input = Self.Fixtures.Day04.sample

    guard let pile = try? ScratchCardParser.parse(input) else {
      XCTFail()
      return
    }

    let expected = 30

    // when
    let actual = pile.captureScores().count

    // then
    XCTAssertEqual(actual, expected)

  }

  func testScratchCardCountWithCopiesForPuzzleData() throws {
    // given
    guard let input = Self.Fixtures.Day04.puzzle else {
      XCTFail()
      return
    }

    guard let pile = try? ScratchCardParser.parse(input) else {
      XCTFail()
      return
    }

    let expected = 6_874_754  // correct answer

    // when
    let actual = pile.captureScores().count

    // then
    XCTAssertEqual(actual, expected)
  }
}
