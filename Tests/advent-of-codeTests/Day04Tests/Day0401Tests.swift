import XCTest

@testable import advent_of_code

final class Puzzle0401Tests: XCTestCase {
  func testCard() {
    let winningNumbers = [
      41, 48, 83, 86, 17,
    ]

    let myNumbers = [
      83, 86, 6, 31, 17, 9, 48, 53,
    ]

    let card = ScratchCard(id: 1, winners: winningNumbers, game: myNumbers)

    XCTAssertEqual(card.id, 1)
    XCTAssertEqual(card.winners, winningNumbers)
    XCTAssertEqual(card.game, myNumbers)

  }

  func testParseDataLine() throws {
    // given
    let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
    let winningNumbers = [
      41, 48, 83, 86, 17,
    ]

    let myNumbers = [
      83, 86, 6, 31, 17, 9, 48, 53,
    ]

    // when
    let parser = ScratchCardParser()
    let card = try parser.parse(input)

    // then
    XCTAssertEqual(card.winners, winningNumbers)
    XCTAssertEqual(card.game, myNumbers)
  }

  func testFindWinningNumbers() throws {
    // given
    var card = ScratchCard(
      id: 1, winners: [41, 48, 83, 86, 17], game: [83, 86, 6, 31, 17, 9, 48, 53])
    let expected = [17, 48, 83, 86]

    // when
    let winningNumbers = card.matches()

    // then
    XCTAssertEqual(winningNumbers.keys.sorted { $0 < $1 }, expected)

  }

  func testScore() throws {
    // given
    var card = ScratchCard(
      id: 1, winners: [41, 48, 83, 86, 17], game: [83, 86, 6, 31, 17, 9, 48, 53]
    )
    let expected = 8

    // when
    let actual = card.score()

    // then
    XCTAssertEqual(actual, expected)
  }

  func testParseSampleCardData() throws {
    // given
    let input = Self.Fixtures.Day04.sample
    let expected = ScratchCardPile(cards: [
      ScratchCard(id: 1, winners: [41, 48, 83, 86, 17], game: [83, 86, 6, 31, 17, 9, 48, 53]),
      ScratchCard(id: 2, winners: [13, 32, 20, 16, 61], game: [61, 30, 68, 82, 17, 32, 24, 19]),
      ScratchCard(id: 3, winners: [1, 21, 53, 59, 44], game: [69, 82, 63, 72, 16, 21, 14, 1]),
      ScratchCard(id: 4, winners: [41, 92, 73, 84, 69], game: [59, 84, 76, 51, 58, 5, 54, 83]),
      ScratchCard(id: 5, winners: [87, 83, 26, 28, 32], game: [88, 30, 70, 12, 93, 22, 82, 36]),
      ScratchCard(id: 6, winners: [31, 18, 13, 56, 72], game: [74, 77, 10, 23, 35, 67, 36, 11]),
    ])

    // when
    let actual = try ScratchCardParser.parse(input)

    // // then
    // XCTAssertEqual(actual, expected)

  }

  func testScoresForSampleCardData() throws {
    // given
    let input = Self.Fixtures.Day04.sample
    let expected = [
      8, 2, 2, 1, 0, 0,
    ]

    // when
    let pile = try ScratchCardParser.parse(input)
    let actual = pile.cards.map { $0.score() }

    // then
    XCTAssertEqual(actual, expected)

  }

  func testWorthForSampleCardData() throws {
    // given
    let input = Self.Fixtures.Day04.sample
    let expected = 13

    // when
    guard let pile = try? ScratchCardParser.parse(input) else {
      XCTFail()
      return
    }
    let actual = pile.worth()

    // then
    XCTAssertEqual(actual, expected)

  }

  func testPuzzleData() throws {
    // given
    guard let input = Self.Fixtures.Day04.puzzle else {
      XCTFail()
      return
    }

    guard let pile = try? ScratchCardParser.parse(input) else {
      XCTFail()
      return
    }

    // correct answer
    let expected = 21088

    // when
    let actual = pile.worth()

    // then
    XCTAssertEqual(actual, expected)

  }

}
