import XCTest

@testable import advent_of_code

final class Puzzle0201Tests: XCTestCase {
  func testSinglePull() throws {
    // given
    let input = "4 red"
    let expected = Pull(count: 4, color: .red)

    // when
    let parser = PullParser()
    let actual = try parser.parse(input)

    XCTAssertEqual(actual, expected)

  }

  func testMultiplePulls() throws {
    // given
    let input = "4 red, 2 blue".split(separator: ",").map { String($0) }
    let expected = [Pull(count: 4, color: .red), Pull(count: 2, color: .blue)]

    // when
    let parser = PullParser()
    let actual = try input.map { try parser.parse($0) }

    // then
    XCTAssertEqual(actual, expected)

  }

  func testStaticPullParser() throws {
    // given
    let input = "4 red, 2 blue".split(separator: ",").map { String($0) }
    let expected = [Pull(count: 4, color: .red), Pull(count: 2, color: .blue)]

    // when
    let actual = try PullParser.parse(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testSingleRoundParser() throws {
    // given
    let input = "4 red, 2 blue"
    let expected = Round(pulls: [Pull(count: 4, color: .red), Pull(count: 2, color: .blue)])

    // when
    let parser = RoundParser()
    let actual = try parser.parse(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testMultipleRoundParser() throws {
    // given
    let input = "3 blue, 4 red; 1 red, 2 green, 6 blue".split(separator: ";").map { String($0) }
    let expected = [
      Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)]),
      Round(pulls: [
        Pull(count: 1, color: .red), Pull(count: 2, color: .green), Pull(count: 6, color: .blue),
      ]),
    ]

    // when
    let parser = RoundParser()
    let actual = try input.map { try parser.parse($0) }

    // then
    XCTAssertEqual(actual, expected)
  }

  func testStaticRoundParser() throws {
    // given
    let input = "3 blue, 4 red; 1 red, 2 green, 6 blue".split(separator: ";").map { String($0) }
    let expected = [
      Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)]),
      Round(pulls: [
        Pull(count: 1, color: .red), Pull(count: 2, color: .green), Pull(count: 6, color: .blue),
      ]),
    ]

    // when
    let actual = try RoundParser.parse(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testStaticRoundParserWithSingleString() throws {
    // given
    let input = "3 blue, 4 red; 1 red, 2 green, 6 blue"
    let expected = [
      Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)]),
      Round(pulls: [
        Pull(count: 1, color: .red), Pull(count: 2, color: .green), Pull(count: 6, color: .blue),
      ]),
    ]

    // when
    let actual = try RoundParser.parse(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testGameIDParser() throws {
    // given
    let input = "Game 1"
    let expected = 1

    // when
    let actual = try GameIDParser.parse(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testSingleGameParser() throws {
    // given
    let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue"
    let expected = Game(
      id: 1,
      rounds: [
        Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)]),
        Round(pulls: [
          Pull(count: 1, color: .red), Pull(count: 2, color: .green), Pull(count: 6, color: .blue),
        ]),
      ]
    )

    // when
    let parser = GameParser()
    let actual = try parser.parse(input)

    // then
    XCTAssertEqual(actual, expected)

  }

  func testMultipleGameParser() throws {
    // given
    let input =
      """
      Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
      Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
      """.split(separator: "\n").map { String($0) }

    let expected = [
      Game(
        id: 1,
        rounds: [
          Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)]),
          Round(pulls: [
            Pull(count: 1, color: .red), Pull(count: 2, color: .green),
            Pull(count: 6, color: .blue),
          ]),
          Round(pulls: [Pull(count: 2, color: .green)]),
        ]
      ),
      Game(
        id: 2,
        rounds: [
          Round(pulls: [Pull(count: 1, color: .blue), Pull(count: 2, color: .green)]),
          Round(pulls: [
            Pull(count: 3, color: .green), Pull(count: 4, color: .blue),
            Pull(count: 1, color: .red),
          ]),
          Round(pulls: [Pull(count: 1, color: .green), Pull(count: 1, color: .blue)]),
        ]
      ),
    ]

    // when
    let actual = try GameParser.parse(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testStaticGameParserWithSingleString() throws {
    // given
    let input =
      """
      Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
      Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
      """

    let expected = [
      Game(
        id: 1,
        rounds: [
          Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)]),
          Round(pulls: [
            Pull(count: 1, color: .red), Pull(count: 2, color: .green),
            Pull(count: 6, color: .blue),
          ]),
          Round(pulls: [Pull(count: 2, color: .green)]),
        ]
      ),
      Game(
        id: 2,
        rounds: [
          Round(pulls: [Pull(count: 1, color: .blue), Pull(count: 2, color: .green)]),
          Round(pulls: [
            Pull(count: 3, color: .green), Pull(count: 4, color: .blue),
            Pull(count: 1, color: .red),
          ]),
          Round(pulls: [Pull(count: 1, color: .green), Pull(count: 1, color: .blue)]),
        ]
      ),
    ]

    // when
    let actual = try GameParser.parse(input)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testBag() {
    // given
    let reds = 3
    let blues = 2
    let greens = 1

    // when
    let bag = Bag(reds: reds, greens: greens, blues: blues)

    // then
    XCTAssertEqual(bag.reds, reds)
    XCTAssertEqual(bag.blues, blues)
    XCTAssertEqual(bag.greens, greens)

  }

  func testIsRoundPossibleGivenBagTrue() {
    // given
    let bag = Bag(reds: 3, greens: 2, blues: 5)
    let round = Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)])
    let expected = false

    // when
    let actuall = round.isPossible(given: bag)

    // then
    XCTAssertEqual(actuall, expected)
  }

  func testIsRoundPossibleGivenBagFalse() {
    // given
    let bag = Bag(reds: 3, greens: 2, blues: 1)
    let round = Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)])
    let expected = false

    // when
    let actuall = round.isPossible(given: bag)

    // then
    XCTAssertEqual(actuall, expected)
  }

  func testIsGamePossibleGivenBagTrue() {
    // given
    let bag = Bag(reds: 7, greens: 2, blues: 5)
    let game = Game(
      id: 1, rounds: [Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)])])
    let expected = true

    // when
    let actual = game.isPossible(given: bag)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testIsGamePossibleGivenBagFalse() {
    // given
    let bag = Bag(reds: 3, greens: 2, blues: 1)
    let game = Game(
      id: 1, rounds: [Round(pulls: [Pull(count: 3, color: .blue), Pull(count: 4, color: .red)])])
    let expected = false

    // when
    let actual = game.isPossible(given: bag)

    // then
    XCTAssertEqual(actual, expected)
  }

  func testWhichGamesArePossible() throws {

    // given
    let bag = Bag(reds: 12, greens: 13, blues: 14)
    let board =
      """
      Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
      Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
      Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
      Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
      Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
      """
    let expected = [1, 2, 5]

    // when
    let games = try GameParser.parse(board)

    let possible = Game.possible(games, given: bag)
    let actual = possible.map { $0.id }

    // then
    XCTAssertEqual(actual, expected)

  }

  func testPuzzleData() throws {

    // given
    let bag = Bag(reds: 12, greens: 13, blues: 14)
    let expected = 2771  // correct answer with current puzzle0201 file

    // when
    guard let board = try? Self.Fixtures.get(file: "puzzle02.txt") else {
      XCTFail("Could Not Get Puzzle Data")
      return
    }

    let games = try GameParser.parse(board)
    let possible = Game.possible(games, given: bag)
    let actual = possible.map { $0.id }.reduce(0, +)

    XCTAssertEqual(actual, expected)
  }

}
