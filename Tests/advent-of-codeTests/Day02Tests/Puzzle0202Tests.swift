import XCTest

@testable import advent_of_code

final class Puzzle0202Tests: XCTestCase {
  func testMinBagNeeded() throws {
    let board =
      """
      Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
      """

    let expected = Bag(reds: 4, greens: 2, blues: 6)

    // when
    let games = try GameParser.parse(board)

    let game = games[0]

    let actual = game.minBag

    // then
    XCTAssertEqual(actual, expected)

  }

  func testGamePower() throws {
    let board =
      """
      Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
      """
    let expected = 48

    // when
    let games = try GameParser.parse(board)

    let game = games[0]

    let actual = game.power

    // then
    XCTAssertEqual(actual, expected)
  }

  func testPuzzleData() throws {

    // given
    let expected = 70924  // correct answer with current puzzle02 file

    // when
    guard let board = try? Self.Fixtures.get(file: "puzzle02.txt") else {
      XCTFail("Could Not Get Puzzle Data")
      return
    }

    let games = try GameParser.parse(board)
    let powers = games.map { $0.power }
    let actual = powers.reduce(0, +)

    XCTAssertEqual(actual, expected)
  }

}
