struct LineTransformer {
  func transform(_ numbers: [Int]) -> Int {
    let strings = numbers.compactMap { String($0) }
    let joined = (strings.first ?? "") + (strings.last ?? "")
    let value = [joined].compactMap { Int($0) }
    return value.first ?? 0
  }
}
