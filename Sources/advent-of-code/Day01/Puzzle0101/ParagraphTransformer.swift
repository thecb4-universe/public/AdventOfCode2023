struct ParagraphTransformer {
  func transform(_ lines: [String]) -> [Int] {
    let lineParser = LineParser()
    let parsedLines = lines.map { lineParser.parse($0) }

    let lineTransformer = LineTransformer()
    let transformedLines = parsedLines.map { lineTransformer.transform($0) }

    return transformedLines
  }
}
