struct ParagraphParser {
  func parse(_ paragraph: String) -> [String] {
    paragraph.split(separator: "\n").map { String($0) }
  }
}
