struct CalibrationDocument {
  let text: String

  init(using text: String) {
    self.text = text
  }

  var values: [Int] {
    let paragraphParser = ParagraphParser()
    let parsedParagraph = paragraphParser.parse(text)

    let paragraphTransformer = ParagraphTransformer()
    let transformedParagraph = paragraphTransformer.transform(parsedParagraph)

    return transformedParagraph
  }
}
