struct LineParser {
  func parse(_ string: String) -> [Int] {
    let split = string.split(separator: "")
    let mapped = split.compactMap { Int($0) }
    return mapped
  }
}
