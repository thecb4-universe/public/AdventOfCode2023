struct TokenCalibrationDocument {
  let text: String

  init(using text: String) {
    self.text = text
  }

  var values: [Int] {
    let paragraphParser = ParagraphParser()
    let parsedParagraph = paragraphParser.parse(text)

    let paragraphTransformer = TokenParagraphTransformer()
    let transformedParagraph = paragraphTransformer.transform(parsedParagraph)

    return transformedParagraph
  }
}
