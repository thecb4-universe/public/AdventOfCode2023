// https://stackoverflow.com/questions/32305891/index-of-a-substring-in-a-string-with-swift

import Foundation

extension StringProtocol {
  func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
    range(of: string, options: options)?.lowerBound
  }
  func indicies<S: StringProtocol>(of string: S) -> [Index] {
    ranges(of: string).map(\.lowerBound)
  }
}
