struct CalibrationValueToken {
  enum Number: String, CaseIterable {
    case one
    case two
    case three
    case four
    case five
    case six
    case seven
    case eight
    case nine
    case n1 = "1"
    case n2 = "2"
    case n3 = "3"
    case n4 = "4"
    case n5 = "5"
    case n6 = "6"
    case n7 = "7"
    case n8 = "8"
    case n9 = "9"

    var int: Int {
      switch self {
        case .one, .n1:
          return 1
        case .two, .n2:
          return 2
        case .three, .n3:
          return 3
        case .four, .n4:
          return 4
        case .five, .n5:
          return 5
        case .six, .n6:
          return 6
        case .seven, .n7:
          return 7
        case .eight, .n8:
          return 8
        case .nine, .n9:
          return 9
      }
    }

    var intString: String {
      String(self.int)
    }

    var string: String {
      self.rawValue
    }

  }

  let number: Number
  let index: String.Index
}

extension CalibrationValueToken.Number: Hashable, Equatable {}

extension CalibrationValueToken: Hashable, Equatable {}
