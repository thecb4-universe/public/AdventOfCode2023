struct TokenParagraphTransformer {
  func transform(_ lines: [String]) -> [Int] {
    let lineParser = TokenLineParser()
    let parsedLines = lines.map { lineParser.parse($0) }

    let lineTransformer = TokenLineTransformer()
    let transformedLines = parsedLines.map { lineTransformer.transform($0) }

    return transformedLines
  }
}
