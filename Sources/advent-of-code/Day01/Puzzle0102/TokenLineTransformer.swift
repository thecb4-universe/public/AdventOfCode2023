struct TokenLineTransformer {
  func transform(_ tokens: [CalibrationValueToken]) -> Int {
    let strings = tokens.map { $0.number.intString }
    let joined = (strings.first ?? "") + (strings.last ?? "")
    let value = [joined].compactMap { Int($0) }
    return value.first ?? 0
  }
}
