struct TokenLineParser {
  func parse(_ string: String) -> [CalibrationValueToken] {
    var tokens = [CalibrationValueToken]()
    for number in CalibrationValueToken.Number.allCases {

      // guard let index = string.index(of: number.rawValue) else { continue }

      // need to use indices as some numbers may appear twice on a line
      // example: qvczktnrfvmgpznspqtsevensevenslmjdqfdld5rrcfour

      let indices = string.lowercased().indicies(of: number.string)

      for index in indices {

        let token = CalibrationValueToken(
          number: number,
          index: index
        )

        tokens.append(token)
      }

    }

    return tokens.sorted { $0.index < $1.index }
  }
}
