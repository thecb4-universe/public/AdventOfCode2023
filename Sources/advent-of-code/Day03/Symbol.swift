enum Symbol: String {
  case asterisk = "*"
  case hash = "#"
  case plus = "+"
  case dollar = "$"
}
