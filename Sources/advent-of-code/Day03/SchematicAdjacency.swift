struct SchematicAdjacency {
  let symbol: SchematicSymbol
  let numbers: [SchematicNumber]
}

extension SchematicAdjacency: Hashable, Equatable {}

extension SchematicAdjacency: CustomStringConvertible {
  var description: String {
    return "\(symbol) :adjacent to: \(numbers)"
  }
}

typealias SchematicAdjacencies = [SchematicAdjacency]

extension SchematicAdjacencies: CustomStringConvertible {
  var description: String {
    return self.map { $0.description }.joined(separator: "\n")
  }
}

extension SchematicAdjacency {
  var isGear: Bool {
    symbol.value == "*" && numbers.count == 2
  }

  var power: Int {
    numbers.map { Int($0.value) ?? 0 }.reduce(1, *)
  }

}
