struct SchematicNumber {
  let value: String
  let range: Range<Int>
}

extension SchematicNumber: Hashable, Equatable {}

extension SchematicNumber: CustomStringConvertible {
  var description: String {
    "\(value) (\(range))"
  }
}
