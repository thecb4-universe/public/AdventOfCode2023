import Foundation

struct Schematic {
  let data: String

  lazy var lines: [String] = {
    data.split(whereSeparator: \.isNewline).map { String($0) }
  }()

  lazy var width: Int = {
    guard let estimatedWidth = lines.first?.count else {
      return 0
    }

    guard lines.allSatisfy({ $0.count == estimatedWidth }) else {
      return 0
    }

    return estimatedWidth
  }()

  lazy var height: Int = {
    lines.count
  }()

  lazy var details: [String] = {
    (lines.map { $0.split(separator: "").map { String($0) } }).flatMap { $0 }
  }()

  lazy var symbols: [String] = {
    Array(
      Set(
        details
          .filter { $0 != .period }
          .filter { !$0.isNumber }
      )
    ).sorted()
  }()

  lazy var symbolMap: [SchematicSymbol] = {
    symbols.flatMap { symbol in
      details.indices(of: symbol)
        .map { Int($0) }
        .map { index in
          SchematicSymbol(value: symbol, index: index)
        }
    }.sorted { $0.index < $1.index }
  }()

  lazy var numbers: [String] = {
    details
      .joined(separator: "")
      .components(separatedBy: CharacterSet.decimalDigits.inverted)
      .filter {
        $0.isNumber
      }
  }()

  lazy var numberMap: [SchematicNumber] = {
    numberReader().map { range, value in
      SchematicNumber(value: value, range: range)
    }.sorted { $0.range.lowerBound < $1.range.lowerBound }
  }()

  lazy var searchMap: [SchematicSearchSpace] = {
    symbolMap.map {
      SchematicSearchSpace(symbol: $0, space: searchSpace(for: $0))
    }
  }()

  lazy var adjacentNumberMap: [SchematicAdjacency] = {
    searchMap.map { search in
      let found = search.space.map { point in
        numberMap.filter { $0.range ~= point }
      }.filter { $0.isEmpty == false }.flatMap { $0 }

      return (search.symbol, Array(Set(found)).sorted { $0.range.lowerBound < $1.range.lowerBound })
    }.map { (symbol, numbers) in
      SchematicAdjacency(symbol: symbol, numbers: numbers)
    }.sorted { $0.symbol.index < $1.symbol.index }
  }()

  lazy var partNumbers: [SchematicNumber] = {
    Array(
      Set(
        adjacentNumberMap.map {
          $0.numbers
        }
        .flatMap { $0 }
      )
    ).sorted { $0.range.lowerBound < $1.range.lowerBound }
  }()

  lazy var partNumbersSum: Int = {
    partNumbers.compactMap { Int($0.value) }.reduce(0, +)
  }()

  mutating func startOfLine(for index: Int) -> Int {
    Int((Double(index) / Double(width)).rounded(.towardZero)) * width
  }

  mutating func endOfLine(for index: Int) -> Int {
    startOfLine(for: index) + width - 1
  }

  mutating func x(for index: Int) -> Int {
    index % width
  }

  mutating func y(for index: Int) -> Int {
    Int((Double(index) / Double(width)).rounded(.towardZero))
  }

  mutating func index(x: Int, y: Int) -> Int {
    y * width + x
  }

  mutating func searchSpace(for symbol: SchematicSymbol) -> [Int] {
    let x = x(for: symbol.index)
    let y = y(for: symbol.index)

    var coordinates = [(Int, Int)]()

    switch (x, y) {
      case (let x, let y) where x > 0 && y > 0:
        coordinates = Self.haloVariances.map { (x + $0.0, y + $0.1) }
      case (let x, let y) where x == 0 && y == 0:
        coordinates = Self.haloVariancesX0Y0.map { (x + $0.0, y + $0.1) }
      case (let x, let y) where x == 0 && y > 0:
        coordinates = Self.haloVariancesX0.map { (x + $0.0, y + $0.1) }
      case (let x, let y) where x > 0 && y == 0:
        coordinates = Self.haloVariancesY0.map { (x + $0.0, y + $0.1) }
      case (let x, let y) where x == self.width - 1 && y == self.height - 1:
        coordinates = Self.haloVariancesXMYM.map { (x + $0.0, y + $0.1) }
      case (let x, let y) where x == self.width - 1 && y < self.height - 1:
        coordinates = Self.haloVariancesXM.map { (x + $0.0, y + $0.1) }
      case (let x, let y) where x < self.width - 1 && y == self.height - 1:
        coordinates = Self.haloVariancesYM.map { (x + $0.0, y + $0.1) }
      case (let x, let y):
        coordinates = Self.haloVariancesDefault.map { (x + $0.0, y + $0.1) }
    }

    let space = coordinates.map { index(x: $0.0, y: $0.1) }

    return space
  }

  mutating func numberReader() -> [Range<Int>: String] {
    var priorValueDigit = false

    var count = 0

    var hash: [Int: [[String: Int]]] = [:]

    var set: [[String: Int]] = []

    for (index, detail) in details.enumerated() {

      if detail.isNumber && !priorValueDigit {
        set.append([detail: index])
      } else {
        priorValueDigit = false
        hash[count] = set
        set = []
        count += 1
      }

      if index == details.count - 1 && !set.isEmpty {
        hash[count] = set
        set = []
      }

      if index == endOfLine(for: index) && !set.isEmpty {
        priorValueDigit = false
        hash[count] = set
        set = []
        count += 1
      }

    }

    let numbers =
      hash
      .filter { !$0.value.isEmpty }
      .sorted(by: { $0.0 < $1.0 })
      .map { $0.value }.map { $0.flatMap { $0.map { $0.key } }.joined() }

    let ranges =
      hash
      .filter { !$0.value.isEmpty }
      .sorted(by: { $0.0 < $1.0 })
      .map { $0.value }.map { $0.flatMap { $0.map { $0.value } } }.map {
        $0.first!..<($0.last! + 1)
      }

    return Dictionary(keys: ranges, values: numbers)
  }

  static let haloVariances = [
    (+1, 0),
    (-1, 0),

    (-1, -1),
    (0, -1),
    (+1, -1),

    (-1, +1),
    (0, +1),
    (+1, +1),
  ]

  static let haloVariancesX0Y0 = [
    (+1, 0),
    (0, +1),
    (+1, +1),
  ]

  static let haloVariancesX0 = [
    (+1, 0),

    (0, -1),
    (+1, -1),

    (0, +1),
    (+1, +1),
  ]

  static let haloVariancesY0 = [
    (+1, 0),
    (-1, 0),

    (-1, +1),
    (0, +1),
    (+1, +1),
  ]

  static let haloVariancesXMYM = [
    (-1, 0),
    (-1, -1),
    (0, -1),
  ]

  static let haloVariancesXM = [
    (-1, 0),

    (-1, -1),
    (0, -1),

    (-1, +1),
    (0, +1),
  ]

  static let haloVariancesYM = [
    (+1, 0),
    (-1, 0),

    (-1, -1),
    (0, -1),
    (+1, -1),

  ]

  static let haloVariancesDefault = [
    (0, 0)
  ]
}

extension Dictionary {
  init(keys: [Key], values: [Value]) {
    self.init()

    for (key, value) in zip(keys, values) {
      self[key] = value
    }
  }
}

extension Array where Element == String {
  var numberMap: [Range<Int>: String] {
    var priorValueDigit = false

    var count = 0

    var hash: [Int: [[String: Int]]] = [:]

    var set: [[String: Int]] = []

    for (index, detail) in self.enumerated() {

      if detail.isNumber && !priorValueDigit {
        set.append([detail: index])
      } else {
        priorValueDigit = false
        hash[count] = set
        set = []
        count += 1
      }

      if index == self.count - 1 && !set.isEmpty {
        hash[count] = set
      }

    }

    let numbers =
      hash
      .filter { !$0.value.isEmpty }
      .sorted(by: { $0.0 < $1.0 })
      .map { $0.value }.map { $0.flatMap { $0.map { $0.key } }.joined() }

    let ranges =
      hash
      .filter { !$0.value.isEmpty }
      .sorted(by: { $0.0 < $1.0 })
      .map { $0.value }.map { $0.flatMap { $0.map { $0.value } } }.map {
        $0.first!..<($0.last! + 1)
      }

    return Dictionary(keys: ranges, values: numbers)
  }
}

extension String {
  static let period: String = "."

  public func allRanges(
    of aString: String,
    options: String.CompareOptions = [],
    range: Range<String.Index>? = nil,
    locale: Locale? = nil
  ) -> [Range<String.Index>] {

    // the slice within which to search
    let slice = (range == nil) ? self[...] : self[range!]

    var previousEnd = self.startIndex
    var ranges = [Range<String.Index>]()

    while let r = slice.range(
      of: aString, options: options,
      range: previousEnd..<self.endIndex,
      locale: locale
    ) {
      if previousEnd != self.endIndex {  // don't increment past the end
        previousEnd = self.index(after: r.lowerBound)
      }
      ranges.append(r)
    }

    return ranges
  }

  public func allSchematicRanges(
    of aString: String,
    options: String.CompareOptions = [],
    range: Range<String.Index>? = nil,
    locale: Locale? = nil
  ) -> [Range<Int>] {
    return allRanges(of: aString, options: options, range: range, locale: locale)
      .map(indexRangeToIntRange)
  }

  private func indexRangeToIntRange(_ range: Range<String.Index>) -> Range<Int> {
    return indexToInt(range.lowerBound)..<indexToInt(range.upperBound)
  }

  private func indexToInt(_ index: String.Index) -> Int {
    return self.distance(from: self.startIndex, to: index)
  }
}

extension Collection where Element: Equatable {
  func indices(of element: Element) -> [Index] { indices.filter { self[$0] == element } }
}
