enum SymbolIndexParserError: Error {
  case invalidInput
}

struct SymbolIndexParser {
  func parse(_ input: String) throws -> [Symbol: [String.Index]] {
    let parser = SymbolParser()
    let symbols = try parser.parse(input)
    return symbols.reduce(into: [:]) { result, symbol in
      result[symbol] = input.indicies(of: symbol.rawValue)
    }
  }
}
