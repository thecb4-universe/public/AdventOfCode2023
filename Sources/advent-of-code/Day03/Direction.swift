enum Direction {

  enum Cardinal {
    case north
    case northWest
    case west
    case southWest
    case south
    case southEast
    case east
    case northEast
  }

  enum Path {
    case left
    case right

    var pathOffset: Int {
      switch self {
        case .left: return -1
        case .right: return 1
      }
    }

  }

}
