struct Shape {
  let width: Int
  let height: Int
  // let startIndex: String.Index
  // let endIndex: String.Index
}

extension Shape: Hashable, Equatable {}
