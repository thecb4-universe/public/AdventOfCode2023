import Foundation

struct SymbolParser {

  func parse(_ input: String) throws -> [Symbol] {
    Array(
      Set(
        input.components(separatedBy: CharacterSet.decimalDigits)
          .joined(separator: "")
          .replacingOccurrences(of: ".", with: "")
          .replacingOccurrences(of: "\n", with: "")
          .split(separator: "")
      )
    )
    .sorted()
    .compactMap { Symbol(rawValue: String($0)) ?? nil }
  }

}
