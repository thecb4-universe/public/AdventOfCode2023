// import Algorithms

enum ShapeParserError: Error {
  case invalidInput
}

struct ShapeParser {
  func parse(_ input: String) throws -> Shape {

    // let lines = input.chunked(by: { $0 == "\n" })
    // print(lines)

    // let lines = input.components(separatedBy: .newlines)

    // guard lines.allSatisfy({ $0.count == lines.first?.count }) else {
    //   throw ShapeParserError.invalidInput
    // }

    // guard let firstInstanceOfNewLine = input.firstIndex(of: "\n") else {
    //   throw ShapeParserError.invalidInput
    // }

    // let startIndex = input.startIndex

    // // + 1 to account for new line
    // let distance = input.distance(from: startIndex, to: firstInstanceOfNewLine) + 1

    // // +1 as the last line does not have a new line character
    // let lineCount = input.filter { $0 == "\n" }.count + 1

    return Shape(
      width: 11,
      height: 10
    )
  }
}
