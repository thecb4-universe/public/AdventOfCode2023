struct SchematicSymbol {
  let value: String
  let index: Int
}

extension SchematicSymbol: Hashable, Equatable {}

extension SchematicSymbol: CustomStringConvertible {
  var description: String {
    return "\(value) (\(index))"
  }
}
