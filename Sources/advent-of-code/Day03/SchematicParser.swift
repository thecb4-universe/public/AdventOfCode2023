enum SchematicParserError: Error {
  case invalidSchematic
}

struct SchematicParser {
  func parse(_ schematic: String) throws -> Shape {
    throw SchematicParserError.invalidSchematic
  }
}
