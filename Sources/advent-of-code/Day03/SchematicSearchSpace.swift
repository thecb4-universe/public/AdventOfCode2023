struct SchematicSearchSpace {
  let symbol: SchematicSymbol
  let space: [Int]
}

extension SchematicSearchSpace: Hashable, Equatable {}

extension SchematicSearchSpace: CustomStringConvertible {
  var description: String {
    return "\(symbol) :: \(space)"
  }
}
