import Foundation

struct DigitReader {
  func read(
    from input: String, starting index: String.Index?, direction: Direction.Path,
    ending endIndex: String.Index
  ) -> Int? {

    print("reading, starting at \(index)")

    let range = input.startIndex..<input.endIndex

    var string = ""

    let offSet = direction.pathOffset

    var possibleIndex = index

    while possibleIndex != nil {
      print(possibleIndex)
      guard let currentIndex = possibleIndex else {
        break
      }

      possibleIndex = input.index(currentIndex, offsetBy: offSet, limitedBy: endIndex)
      // print(possibleIndex)
    }

    // var currentIndex = input.index(
    //   index, offsetBy: offSet)

    // print(index)

    // while range.contains(currentIndex) {

    //   guard
    //     input[currentIndex].isNumber,
    //     let value = Int(String(input[currentIndex]))
    //   else {
    //     break
    //   }

    //   print(currentIndex)
    //   print(input[currentIndex])
    //   string = String(value) + string
    //   currentIndex = input.index(currentIndex, offsetBy: offSet, limitedBy: endIndex)
    // }

    // while (input.startIndex..<input.endIndex).contains(currentIndex) {
    //   currentIndex = input.index(currentIndex, offsetBy: offSet)
    // }

    // while (input.startIndex..<input.endIndex).contains(currentIndex) {
    //   guard !input[currentIndex].isNumber else {
    //     continue
    //   }

    //   string = String(input[currentIndex]) + string

    //   print(string)

    //   let nextIndex = input.index(
    //     currentIndex, offsetBy: offSet)

    //   currentIndex = nextIndex
    // }

    if direction == .right {
      string = String(string.reversed())
    }

    return Int(string)
  }

  func isValidNumber(_ input: String?) -> Bool {
    guard let string = input else { return false }
    return string.isNumber
  }

}

extension String {
  public var isNumber: Bool {
    return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits) != nil
      && self.rangeOfCharacter(from: CharacterSet.letters) == nil
  }
}

extension Collection where Indices.Iterator.Element == Index {
  subscript(safe index: Index) -> Iterator.Element? {
    return indices.contains(index) ? self[index] : nil
  }
}

extension StringProtocol {
  func contains(_ index: String.Index) -> Bool {
    return index >= startIndex && index < endIndex
  }
}
