enum GameParserError: Error {
  case invalidInput
}

struct GameParser {
  func parse(_ input: String) throws -> Game {

    let gameInfo = input.split(separator: ":").map(String.init)

    guard
      let potentialID = gameInfo.first,
      let potentialRounds = gameInfo.last
    else {
      throw GameParserError.invalidInput
    }

    let id = try GameIDParser.parse(potentialID)
    let rounds = try RoundParser.parse(potentialRounds)

    return Game(id: id, rounds: rounds)
  }

  static func parse(_ inputs: [String]) throws -> [Game] {
    let parser = GameParser()

    return try inputs.map { try parser.parse($0) }
  }

  static func parse(_ input: String) throws -> [Game] {
    let potentialGames = input.split(separator: "\n").map { String($0) }
    return try parse(potentialGames)
  }
}
