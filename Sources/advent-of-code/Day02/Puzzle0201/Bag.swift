struct Bag {
  let reds: Int
  let greens: Int
  let blues: Int
}

extension Bag: Hashable, Equatable {}
