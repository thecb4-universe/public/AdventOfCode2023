enum PullParserError: Error {
  case invalidInput
}

struct PullParser {

  // parsing `4 red`
  func parse(_ input: String) throws -> Pull {

    let pullInfo = input.split(separator: " ").map { String($0) }

    guard
      let countString = pullInfo.first,
      let count = Int(countString),
      let colorString = pullInfo.last,
      let color = Color(rawValue: colorString)
    else {
      throw PullParserError.invalidInput
    }

    return Pull(count: count, color: color)
  }

  static func parse(_ inputs: [String]) throws -> [Pull] {

    let parser = PullParser()

    return try inputs.map { try parser.parse($0) }

  }
}
