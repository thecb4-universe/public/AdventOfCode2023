struct Pull {
  let count: Int
  let color: Color
}

extension Pull: Hashable, Equatable {}
