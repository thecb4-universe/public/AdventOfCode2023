struct Round {
  let pulls: [Pull]

  init(pulls: [Pull] = []) {
    self.pulls = pulls
  }
}

extension Round: Hashable, Equatable {}

extension Round {
  var reds: Int {
    pulls.filter { $0.color == .red }.reduce(
      0,
      { sum, values in
        sum + values.count
      })
  }

  var greens: Int {
    pulls.filter { $0.color == .green }.reduce(
      0,
      { sum, values in
        sum + values.count
      })
  }

  var blues: Int {
    pulls.filter { $0.color == .blue }.reduce(
      0,
      { sum, values in
        sum + values.count
      })
  }

  func isPossible(given bag: Bag) -> Bool {
    reds <= bag.reds && greens <= bag.greens && blues <= bag.blues
  }
}
