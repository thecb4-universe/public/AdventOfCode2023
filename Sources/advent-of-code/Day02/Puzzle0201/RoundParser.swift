struct RoundParser {

  func parse(_ input: String) throws -> Round {

    let potentalPulls = input.split(separator: ",").map { String($0) }

    let pulls = try PullParser.parse(potentalPulls)

    return Round(pulls: pulls)

  }

  static func parse(_ inputs: [String]) throws -> [Round] {

    let parser = RoundParser()

    return try inputs.map { try parser.parse($0) }
  }

  static func parse(_ input: String) throws -> [Round] {
    let potentialRounds = input.split(separator: ";").map { String($0) }
    return try parse(potentialRounds)
  }
}
