struct Game {
  let id: Int
  let rounds: [Round]
}

extension Game: Hashable, Equatable {}

extension Game {
  func isPossible(given bag: Bag) -> Bool {
    rounds.allSatisfy { $0.isPossible(given: bag) }
  }

  static func possible(_ games: [Game], given bag: Bag) -> [Game] {
    games.filter { $0.isPossible(given: bag) }
  }
}
