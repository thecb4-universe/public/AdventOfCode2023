enum GameIDParserError: Error {
  case invalidInput
}

struct GameIDParser {

  static func parse(_ input: String) throws -> Int {
    guard
      let potentialID = input.split(separator: " ").last,
      let id = Int(potentialID)
    else {
      throw GameIDParserError.invalidInput
    }

    return id
  }
}
