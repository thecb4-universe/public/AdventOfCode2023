extension Game {
  var reds: [Int] {
    rounds.flatMap { $0.pulls.filter { $0.color == .red } }.map { $0.count }
  }

  var greens: [Int] {
    rounds.flatMap { $0.pulls.filter { $0.color == .green } }.map { $0.count }
  }

  var blue: [Int] {
    rounds.flatMap { $0.pulls.filter { $0.color == .blue } }.map { $0.count }
  }

  var minBag: Bag {
    let red = reds.max() ?? 0
    let green = greens.max() ?? 0
    let blue = blue.max() ?? 0
    return Bag(reds: red, greens: green, blues: blue)
  }

  var power: Int {
    minBag.reds * minBag.greens * minBag.blues
  }

}
