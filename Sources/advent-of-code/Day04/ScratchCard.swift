struct ScratchCard {
  let id: Int
  let winners: [Int]
  let game: [Int]

  func matches() -> [Int: Int] {
    Dictionary(
      uniqueKeysWithValues:
        winners.map { winner in
          (winner, game.filter { $0 == winner }.count)
        }.filter { $0.1 > 0 }
    )
  }

  func score() -> Int {
    if matches().keys.count == 0 {
      0
    } else {
      2 ^^ (matches().values.reduce(0, +) - 1)
    }
  }

  func winningCopies() -> [Int] {

    if matches().keys.count == 0 {
      [Int]()
    } else {
      Array(Set((id + 1)...(id + matches().keys.count))).sorted { $0 < $1 }
    }
  }

  func keepScore() -> ScoreKeeper {
    ScoreKeeper(id: id, score: score(), copies: winningCopies())
  }

}

extension ScratchCard: Hashable, Equatable {}

extension ScratchCard: CustomStringConvertible {
  var description: String {
    """
    ScratchCard(
      id: \(id),
      winners: \(winners),
      game: \(game),
      score: \(score())
      winning copies: \(winningCopies())
    )
    """
  }
}

extension DefaultStringInterpolation {
  mutating func appendInterpolation(_ cards: [ScratchCard]) {
    appendLiteral(cards.map { "  \($0)" }.joined(separator: "\n"))
  }
}
