struct ScoreKeeper {
  let id: Int
  let score: Int
  let copies: [Int]
}
