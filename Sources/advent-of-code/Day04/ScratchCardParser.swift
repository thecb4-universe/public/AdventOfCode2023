enum ScratchCardParserError: Error {
  case invalidInput
}

struct ScratchCardParser {
  func parse(_ input: String) throws -> ScratchCard {

    let metas = input.components(separatedBy: ":")

    guard let number = metas.first?.components(separatedBy: " ").last else {
      throw ScratchCardParserError.invalidInput
    }

    guard let id = Int(number) else {
      throw ScratchCardParserError.invalidInput
    }

    guard let sets = metas.last?.components(separatedBy: "|") else {
      throw ScratchCardParserError.invalidInput
    }

    guard let winnerStrings = sets.first?.components(separatedBy: " ") else {
      throw ScratchCardParserError.invalidInput
    }

    let winners = winnerStrings.compactMap { Int($0) }

    guard let numberStrings = sets.last?.components(separatedBy: " ") else {
      throw ScratchCardParserError.invalidInput
    }

    let numbers = numberStrings.compactMap { Int($0) }

    return ScratchCard(
      id: id,
      winners: winners,
      game: numbers
    )

  }

  static func parse(_ input: String) throws -> ScratchCardPile {
    let lines = input.components(separatedBy: "\n")

    let parser = ScratchCardParser()
    let cards = try lines.map { try parser.parse($0) }

    return ScratchCardPile(cards: cards)
  }

}
