struct ScratchCardPile {
  let cards: [ScratchCard]

  func worth() -> Int {
    cards.reduce(0) { $0 + $1.score() }
  }

  func winningCopyMap() -> [Int: [Int]] {
    cards.reduce(into: [:]) { mapping, card in
      mapping[card.id] = card.winningCopies()
    }
  }

  func captureScores(for keeper: ScoreKeeper, in keepers: [ScoreKeeper]) -> [Int] {

    if keeper.copies.isEmpty {
      return [keeper.score]
    } else {
      return [keeper.score]
        + keeper.copies
        .map { keepers[$0 - 1] }
        .flatMap { captureScores(for: $0, in: keepers) }
    }

  }

  func captureScores() -> [Int] {
    let keepers = cards.map { $0.keepScore() }
    return keepers.flatMap { captureScores(for: $0, in: keepers) }
  }

  func worthWithCopies() -> Int {
    captureScores().reduce(0, +)
  }

}

extension ScratchCardPile: Hashable, Equatable {}

extension ScratchCardPile: CustomStringConvertible {

  var description: String {
    """
    Pile: {
      cards: [
        \(cards)
      ]
      worth: \(worth())
      winningCopyMap: \(winningCopyMap())
      worth with copies: \(worthWithCopies())
    }
    """
  }
}
