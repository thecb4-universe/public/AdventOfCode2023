struct Almanac {
  let seeds: [Int]
  let maps: [AlmanacMap]
}

extension Almanac {
  func destination(from input: Int, mapping header: MapHeader) -> Int {
    let map = maps.first(where: { $0.header == header })!
    return map.destination(from: input)
  }

  func location(for seed: Int) -> Int {
    let soil = destination(from: seed, mapping: .seed_soil)

    let fertilizer = destination(from: soil, mapping: .soil_fertilizer)

    let water = destination(from: fertilizer, mapping: .fertilizer_water)

    let light = destination(from: water, mapping: .water_light)

    let temperature = destination(from: light, mapping: .light_temperature)

    let humidity = destination(from: temperature, mapping: .temperature_humidity)

    let location = destination(from: humidity, mapping: .humidity_location)

    return location
  }

  var bestSeed: Int {
    (seeds.map { location(for: $0) }).min() ?? -1
  }
}

extension Almanac: Hashable, Equatable {}

extension Almanac: CustomStringConvertible {
  var description: String {
    """
    Almanac: 
      seeds: \(seeds)
      maps: \(maps)
    """
  }
}
