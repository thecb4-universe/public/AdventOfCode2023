enum MapHeader: String {
  case seed_soil = "seed-to-soil map"
  case soil_fertilizer = "soil-to-fertilizer map"
  case fertilizer_water = "fertilizer-to-water map"
  case water_light = "water-to-light map"
  case light_temperature = "light-to-temperature map"
  case temperature_humidity = "temperature-to-humidity map"
  case humidity_location = "humidity-to-location map"
}

extension MapHeader: CustomStringConvertible {
  var description: String {
    return rawValue
  }
}
