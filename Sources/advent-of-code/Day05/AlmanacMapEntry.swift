struct AlmanacMapEntry {
  let source: ClosedRange<Int>
  let destination: ClosedRange<Int>
}

extension AlmanacMapEntry: Hashable, Equatable {}

extension AlmanacMapEntry: CustomStringConvertible {
  var description: String {
    return "MapEntry: \(source) -> \(destination)"
  }
}

extension DefaultStringInterpolation {
  mutating func appendInterpolation(_ entry: [AlmanacMapEntry]) {
    appendInterpolation(entry.map { "- \($0)" }.joined(separator: "\n  "))
  }
}
