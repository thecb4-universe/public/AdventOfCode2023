struct AlmanacParser {

  func parseSeeds(_ input: String) throws -> [Int] {
    guard let seedsInfo = input.components(separatedBy: ":").dropFirst().last else {
      throw AlmanacParserError.invalidInput(input)
    }

    return seedsInfo.components(separatedBy: " ").compactMap { Int($0) }
  }

  func parseSourceRange(_ input: String) throws -> ClosedRange<Int> {
    let rangeInfo = input.components(separatedBy: " ").compactMap { Int($0) }

    guard rangeInfo.count == 3 else {
      throw AlmanacParserError.invalidInput(input)
    }

    let start = rangeInfo[1]
    let length = rangeInfo[2]

    return start...(start + length - 1)

  }

  func parseDestinationRange(_ input: String) throws -> ClosedRange<Int> {
    let rangeInfo = input.components(separatedBy: " ").compactMap { Int($0) }

    guard rangeInfo.count == 3 else {
      throw AlmanacParserError.invalidInput(input)
    }

    let start = rangeInfo[0]
    let length = rangeInfo[2]

    return start...(start + length - 1)

  }

  func parseMapEntry(_ input: String) throws -> AlmanacMapEntry {
    let source = try parseSourceRange(input)
    let destination = try parseDestinationRange(input)
    return AlmanacMapEntry(source: source, destination: destination)
  }

  func parseHeader(_ input: String) throws -> MapHeader {
    let headerInfo = input.replacingOccurrences(of: ":", with: "")
    guard let header = MapHeader(rawValue: headerInfo) else {
      throw AlmanacParserError.invalidInput(input)
    }
    return header
  }

  func parseMap(_ input: String) throws -> AlmanacMap {

    let lines = input.components(separatedBy: "\n")

    guard let headerInfo = lines.first else {
      throw AlmanacParserError.invalidInput(input)
    }

    let header = try parseHeader(headerInfo)

    let mappings = Array(lines[1..<lines.count])

    let maps =
      try mappings
      .map {
        AlmanacMapEntry(
          source: try parseSourceRange($0),
          destination: try parseDestinationRange($0)
        )
      }

    return AlmanacMap(header: header, entries: maps)

  }

  func parse(_ input: String) throws -> Almanac {
    let almanacInfo = input.components(separatedBy: "\n\n")

    guard !almanacInfo.isEmpty else {
      throw AlmanacParserError.invalidInput(input)
    }

    let seedInfo = almanacInfo[0]

    let seeds = try parseSeeds(seedInfo)

    let mapInfo = almanacInfo[1..<almanacInfo.count]

    let maps = try mapInfo.map { try parseMap($0) }

    return Almanac(seeds: seeds, maps: maps)
  }
}

enum ParserMethod: String {
  case one
}

enum AlmanacParserError: Error {
  case invalidInput(String)
}
