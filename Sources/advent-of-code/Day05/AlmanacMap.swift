struct AlmanacMap {
  let header: MapHeader
  let entries: [AlmanacMapEntry]
}

extension AlmanacMap {
  func destination(from number: Int) -> Int {
    guard let entry = (entries.filter { $0.source.contains(number) }).first else {
      return number
    }
    let distance = number - entry.source.lowerBound
    return entry.destination.lowerBound + distance
  }
}

extension AlmanacMap: Hashable, Equatable {}

extension AlmanacMap: CustomStringConvertible {
  var description: String {
    """

    AlmanacMap:
      header: \(header)
      entries: 
      \(entries)
    """
  }
}
